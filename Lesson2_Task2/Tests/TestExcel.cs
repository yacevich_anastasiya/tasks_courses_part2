﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.OleDb;
using Dapper;
using System.IO;
using Lesson2_Task2.Utils;
using NUnit.Framework;


namespace Lesson2_Task2.Tests
{
    class TestExcel
    {
        public static string TestDataFileConnection()
        {
            var fileName =  ConfigurationManager.AppSettings["TestDataSheetPath"];
            string path = Path.Combine(Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.FullName,
                    fileName);
            var con = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source = {0}; Extended
Properties=Excel 12.0;", path);
            return con;
        }
        public static Utils.TestCaseData GetTestData(string keyName)
        {
            using (var connection = new OleDbConnection(TestDataFileConnection()))
            {
                connection.Open();
                var query = string.Format("select * from [LogInDataset$] where key='{0}'", keyName);
                var value = connection.Query<Utils.TestCaseData>(query).FirstOrDefault();
                connection.Close();
                return value;
            }
        }
    }
}
