﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lesson2_Task2.Pages;
using NUnit.Framework;

namespace Lesson2_Task2.Tests
{
    [TestFixture]
    [Parallelizable]
    class LogInTests
    {
        private MainPage mainPage;

        [SetUp]
        public void Setup()
        {
            
        }

        [TestCase("NegativeCase1")]
        [TestCase("NegativeCase2")]
        public void LoginFalse(string testCase)
        {
            
            var userDate = TestExcel.GetTestData(testCase);
            bool isIncorrect = mainPage.LogIn(userDate.UserName, userDate.Password, true);
            Assert.True(isIncorrect, 
                string.Format("Error {0} - {1}", userDate.UserName, userDate.Password));

        }

        [OneTimeTearDown]
        public  void CloseDriver()
        {
            mainPage.CloseBrowser();
        }

    }
}
