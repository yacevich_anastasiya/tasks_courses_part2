﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.Events;
using OpenQA.Selenium.Support.Extensions;

namespace Lesson2_Task2.Utils
{
    class WebDriverFactory
    {
      //  private EventFiringWebDriver events;
      public  IWebDriver driver;
        public IWebDriver CreateDriver(string browserType)
        {
            EventFiringWebDriver firingDriver = null;
            switch (browserType)
            {
                case "Chrome":
                  //  return new ChromeDriver();
                  firingDriver = new EventFiringWebDriver(new ChromeDriver());
                    break;
                case "Firefox":
                    //return  new FirefoxDriver();
                     firingDriver = new EventFiringWebDriver(new FirefoxDriver());
                    break;
                default:
                //    return null;
                break;
            }
            firingDriver.ExceptionThrown += TakeScreenShotForException;
            driver = firingDriver;
            return driver;
        }

        public void TakeScreenShotForException(object o, WebDriverExceptionEventArgs e)
        {
            string s = DateTime.Now.ToString("D");
            driver.TakeScreenshot().SaveAsFile("exception "+ s +".png",ScreenshotImageFormat.Png);
        }
    }
}
