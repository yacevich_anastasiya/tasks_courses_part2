﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson2_Task2.Utils
{
   public class TestCaseData
    {
        public string Key { get; set; }
        public  string UserName { get; set; }
        public  string Password { get; set; }
    }
}
