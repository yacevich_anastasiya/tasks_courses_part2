﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson2_Task2.Utils
{
    public  class Config
    {
        public static string BrowserType
        {
            get { return ConfigurationManager.AppSettings["BrowserType"]; }
        }
        public static string TestDataSheetPath
        {
            get
            {
                return ConfigurationManager.AppSettings["TestDataSheetPath"];
            }
        }

    }
}
