﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using NLog;

namespace Lesson2_Task2.Utils
{
    class WebDriverRunner
    {
        private static IWebDriver driver;
        private static Logger log = LogManager.GetCurrentClassLogger();

       private  WebDriverRunner()
        {
            WebDriverFactory factory = new WebDriverFactory();
            log.Debug("Create webdriver");
            driver = factory.CreateDriver("Chrome");
            log.Debug("Set time for run {0} sec", 10);
           // driver = new ChromeDriver();
            driver.Manage().Timeouts().ImplicitWait= TimeSpan.FromSeconds(50);
        }

        public static IWebDriver GetDriver()
        {
            if (driver == null)// для параллельного запуска закомментить
            {//
                new WebDriverRunner();
            }//
            return driver;
        }

    }
}
