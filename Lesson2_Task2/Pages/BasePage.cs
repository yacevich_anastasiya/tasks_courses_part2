﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lesson2_Task2.Utils;
using OpenQA.Selenium;

namespace Lesson2_Task2.Pages
{
   public class BasePage
   {
       protected IWebDriver driver;
        

       public BasePage()
       {
           driver = WebDriverRunner.GetDriver();
           
       }
       public void CloseBrowser()
       {
           driver.Close();
       }
   }
}
