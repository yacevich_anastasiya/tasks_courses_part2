﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;

namespace Lesson2_Task2.Pages
{
    public class MainPage:BasePage
    {
        string url = "https://www.kinopoisk.ru";
        private const string _loginButtonXpath = "//a[text()='Войти на сайт']";
        private const string _authFrameXpath = "";
        private const string _loginFieldXpath = "";
        private const string _passwordFieldXpath = "";


        public bool LogIn(string userName, string password, bool checkFailure)
        {
            driver.TakeScreenshot().SaveAsFile("123.jpeg", ScreenshotImageFormat.Jpeg);
            driver.FindElement(By.XPath(_loginButtonXpath)).Click();
            driver.SwitchTo().Frame(driver.FindElement(By.XPath(_authFrameXpath)));
            driver.FindElement(By.XPath(_loginFieldXpath)).SendKeys(userName);
            driver.FindElement(By.XPath(_passwordFieldXpath)).SendKeys(password);
            return checkFailure;
        }
    }
}
