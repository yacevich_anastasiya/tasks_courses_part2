﻿using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using  OpenQA.Selenium.Interactions;


namespace Lesson2_Task2
{
    [TestFixture]
    class Lesson3KinopoiskSearch
    {
        IWebDriver driver;
        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            // driver = new ChromeDriver();
            // IWebDriver driver = new FirefoxDriver();
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("start-maximized");
            options.LeaveBrowserRunning = true;
            options.AddUserProfilePreference("intl.accept_languages", "nl");
            driver = new ChromeDriver(options);
        }

        [SetUp]
        public void Setup()
        {
            driver.Url = "https://www.kinopoisk.ru";
            driver.Manage().Timeouts().ImplicitWait=TimeSpan.FromSeconds(5600);
            Thread.Sleep(3000);
        }
        [Test]
        [TestCase("мгла")]
        [TestCase("челюсти")]
        public void LessonKinopoiskSearch (string name)
        {
            Console.WriteLine(driver.Title);
            /*  Thread.Sleep(3000);
              driver.Navigate().GoToUrl("http://192.168.1.50:8080/job/Portal/view/Swc_Portal/");
              Thread.Sleep(3000);
              driver.Navigate().Back();*/
            // IWebElement SearchField = driver.FindElement(By.XPath("//form/input[1]"));
            IWebElement SearchField1 = driver.FindElement(By.XPath("//form/input[@name='kp_query']"));
            SearchField1.SendKeys(name);
            IWebElement searchButton = driver.FindElement(By.XPath("//input[@value = 'искать!']"));
            searchButton.Click();
            IWebElement may = driver.FindElement(By.XPath("//*[@id='block_left_pad']/div/div[3]/p"));
            Assert.True(may.Displayed);
            Thread.Sleep(3000);
        }

        [Test]
        public void Lesson4()
        {
            Actions action = new Actions(driver);
            IWebElement click= driver.FindElement(By.XPath("//a[text()='Войти на сайт']"));
            action.ContextClick(click).Perform();
            action.Click(click).Perform();
            driver.SwitchTo().Frame("kp2-authapi-iframe");
            IWebElement login = driver.FindElement(By.XPath("//input[@name='login']"));
            //login.Click();
            login.SendKeys("qwerty");
            login.SendKeys(Keys.Tab);
            Thread.Sleep(3000);
            Console.WriteLine("login.Text = " + login.Text);
    //        Assert.True(login.Text=="qwerty");
        

        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            driver.Close();
        }
    }
}
