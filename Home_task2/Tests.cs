﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.IO;

namespace Home_task2
{
    /*
    [TestFixture]
    class Tests
    {
        private IWebDriver driver;
        private WebDriverWait wait;
        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            driver = new ChromeDriver();
        }

        [SetUp]
        public void Setup()
        {
            driver.Url = "https://www.kinopoisk.ru";
             wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        }

        [Test]
        [TestCase("осьминог")]
        [TestCase("кактус")]
        public void KinopoiskTests(string name)
        {
             
            IWebElement filmName = driver.FindElement(By.XPath("//input[@name = 'kp_query']"));
            filmName.Clear();
            filmName.SendKeys(name);
            IWebElement filmSearch = driver.FindElement(By.XPath("//input[@value='искать!']"));
            filmSearch.Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[contains(text(), 'показать все')]")));
            driver.FindElement(By.XPath("//a[contains(text(), 'показать все')]")).Click();

            IList<IWebElement> ListOfFilms = driver.FindElements(By.XPath("//div/p[@class='name']/a[contains(text(), '"+name+"')]"));

            for (int i = 0; i < ListOfFilms.Count; i++)

            {
                StreamWriter sw = new StreamWriter("fileForDescription.txt", true);
                IList<IWebElement> RefreshListOfFilms = driver.FindElements(By.XPath("//div/p[@class='name']/a[contains(text(), '" + name + "')]"));
                RefreshListOfFilms[i].Click();

                try
                {
                    IWebElement description = driver.FindElement(By.XPath("//span/div[@itemprop='description']"));

                    sw.WriteLine(description.Text);
                }
                catch (Exception)

                { Console.WriteLine("no any description"); }

                driver.Navigate().Back();
               // WebDriverWait wait1 = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                sw.Close();
                IWebElement may = driver.FindElement(By.XPath("//b[1][text()='"+name+"']"));
                Assert.True(may.Displayed);
            }
        }
        [Test]
        public void TestLogin()
        {
            IWebElement login = driver.FindElement(By.XPath("//span/a[1][text()='Войти на сайт']"));
            login.Click();
            driver.SwitchTo().Frame("kp2-authapi-iframe");
            IWebElement loginName = driver.FindElement(By.XPath("//input[@name='login']"));
            loginName.Click();
            loginName.SendKeys("testLogin7");
            IWebElement pass = driver.FindElement(By.XPath("//input[@name='password']"));
            pass.Click();
            pass.SendKeys("testLogin");
            IWebElement enter = driver.FindElement(By.XPath("//span[text()='Вход']/.."));
            enter.Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//span/input[1]")));
            IWebElement profile = driver.FindElement(By.XPath("//span/a[2]"));
            Assert.True(profile.Text.Contains("testLogin7"));
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            driver.Close();
        }
    }*/
}

