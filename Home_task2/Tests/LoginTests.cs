﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Home_task2.Pages;
using OpenQA.Selenium;
using Home_task2.Utils;

namespace Home_task2.Tests
{
    [TestFixture]
    class LoginTests
    {
        MainPage mainPage;
        private const string loginEmail = "testLogin7";
        private const string password = "testLogin";

        [SetUp]
        public void SetUp()
        {
            mainPage = new MainPage();
        }

        [TestCase("Case1")]
        public void ValidLoginTest(string testCase)
        {
            var getData = ExcelDataAccess.GetTestData(testCase);
            IWebElement profile = mainPage.Login(getData.username, getData.password);
            Assert.True(profile.Text.Contains(getData.username));
        }
        [OneTimeTearDown]
        public void CloseDriver()
        {
            mainPage.CloseBrowser();
        }
    }
}
