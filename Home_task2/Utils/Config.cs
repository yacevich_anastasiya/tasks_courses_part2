﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Home_task2.Utils
{
    class Config
    {
        public static string BrowserType
        {
            get
            {
                return ConfigurationManager.AppSettings["BrowserType"];
            }
        }

        public static string ExcelFile
        {
            get
            {
                return ConfigurationManager.AppSettings["ExcelFile"];
            }
        }
    }
}
