﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Home_task2.Utils
{
   class WebDriverRunner
    {
        private static IWebDriver driver;
        private WebDriverRunner()
        {
            WebDriverFactory factory = new WebDriverFactory();
            driver = factory.DriverType(Config.BrowserType);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
         } 
        public static IWebDriver GetDriver()
        {
            if (driver == null)
            {
                new WebDriverRunner();
            }
            return driver;
        }
    }
}
