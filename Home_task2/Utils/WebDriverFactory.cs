﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;


namespace Home_task2.Utils
{
    class WebDriverFactory
    {
        public IWebDriver DriverType(string driverType)
        {
            switch(driverType)
            {
                case "Chrome":
                    return new ChromeDriver();
                case "FireFox":
                    return new FirefoxDriver();
                default:
                    return null;

            }
        }
    }
}
