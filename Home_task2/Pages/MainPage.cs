﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Home_task2.Pages
{
    public class MainPage : BasePage
    {
        private string url = "https://www.kinopoisk.ru";
        private const string loginButtonXpath = "//span/a[1][text()='Войти на сайт']";
        private const string frameLoginXpath = "kp2-authapi-iframe";
        private const string loginNameXpath = "//input[@name='login']";
        private const string passwordXpath = "//input[@name='password']";
        private const string enterXpath = "//span[text()='Вход']/..";
    
        public MainPage()
        {
            driver.Url = url;
        }
        public IWebElement Login(string username, string password)
        {
            driver.FindElement(By.XPath(loginButtonXpath)).Click();
            FillEmailField(username);
            driver.FindElement(By.XPath(passwordXpath)).SendKeys(password);
            IWebElement enterButton = driver.FindElement(By.XPath(enterXpath));

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            wait.Until(ExpectedConditions.ElementToBeClickable(enterButton)); 
            enterButton.Click();
            //wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//span/input[1]")));
            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//div/a[contains(@class, 'header-fresh-user-partial-component__button')]")));
            //IWebElement profile = driver.FindElement(By.XPath("//span/a[2]"));
            IWebElement loginButton = driver.FindElement(By.XPath("//div/a[contains(@class, 'header-fresh-user-partial-component__button')]"));
            loginButton.Click();
            IWebElement profile = driver.FindElement(By.XPath("//div[1][contains(@class, 'nick_name')]"));
            return profile;

        }
        public MainPage FillEmailField(string username)
        {
            driver.SwitchTo().Frame(frameLoginXpath);
            driver.FindElement(By.XPath(loginNameXpath)).SendKeys(username);
            return this;
        }

        public string GetEmailField()
        {
            return driver.FindElement(By.XPath(loginNameXpath)).Text;
        }


    }
}
