﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using Home_task2.Utils;


namespace Home_task2.Pages
{
    public class BasePage
    {
        protected IWebDriver driver;
        public BasePage()
        {
           driver = WebDriverRunner.GetDriver();
        }
        public void CloseBrowser()
        {
            driver.Close();
        }
    }
}
